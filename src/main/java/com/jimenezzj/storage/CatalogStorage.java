package com.jimenezzj.storage;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.jimenezzj.models.Catalog;
import com.jimenezzj.models.Client;
import com.jimenezzj.models.Shirt;
import com.jimenezzj.models.ShirtSize;
import com.jimenezzj.util.Storage;

/**
 * Representa un repositorio de datos que puede realizar acciones sobre el
 * centro de origen de los datos
 *
 * @author Jonathan Jimenez Zambrana
 * @since 1.0
 */
public class CatalogStorage {

    private final String CATALOGS_FILE = "catalogs.txt";

    /**
     * Agrega un nuevo ctalogo al origen de los datos
     *
     * @param pNewCatalog el nuevo catalogo que se ve a a agregar
     * @throws IOException al ingresar un dato erroneo o invalido
     */
    public void addCatalog(Catalog pNewCatalog) throws IOException {
        var newCatalog = addDefaults(pNewCatalog);
        Storage.writeInFile(CATALOGS_FILE, newCatalog.toString());
    }

    /**
     * Obtiene todos los catalogos desde el origen de los datos
     *
     * @return lista de tipo {@link com.jimenezzj.models.Catalog} con todos los
     * catalogs
     * @throws IOException al ingresar un dato erroneo o invalido
     */
    public List<Catalog> fetchAllCatalogs() throws IOException {
        Path tmpPath = Paths.get(Storage.getFilesDir().concat(CATALOGS_FILE));
        return Files.lines(tmpPath).filter(l -> !l.isBlank())
                .map(l -> {
                    List<String> datos = Storage.getDataFromString(l, "Catalogo");

                    String[] test = datos.get(2).split(" ");
                    List<String> at = Storage.createStringChunksFromArray(test, 5);

                    List<Shirt> shirtsList = Storage.createShirtsList(at);
                    return new Catalog(datos.get(0), LocalDate.parse(datos.get(1)), shirtsList);
                }).collect(Collectors.toList());
    }

    private Catalog addDefaults(Catalog pCatalog) throws IOException {
        String newCatalogName = pCatalog.getName();
        String nameForToDay = LocalDate.now().getMonth().toString().substring(0, 1).toUpperCase()
                + LocalDate.now().getMonth().toString().substring(1).toLowerCase() + "-" + LocalDate.now().getYear();
        for (Catalog c : fetchAllCatalogs()) {
            if (c.getName().equals(newCatalogName)) {
                int totalToday = (int) fetchAllCatalogs().stream().filter(catalog ->
                        catalog.getName().toLowerCase().contains(nameForToDay.toLowerCase()))
                        .count() + 1;
                pCatalog.setName(newCatalogName + "-" + totalToday);
            }
        }
        return pCatalog;
    }


}
