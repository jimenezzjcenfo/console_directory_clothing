package com.jimenezzj.storage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import com.jimenezzj.models.Client;
import com.jimenezzj.util.Storage;


/**
 * Representa un repositorio de datos que puede realizar acciones sobre el
 * centro de origen de los datos, relacionado con los clientes
 */
public class ClientStorage {


    private final String CLIENTS_FILE = "clients.txt";

    /**
     * Agregar un nuevo cliente al origen de los datos
     *
     * @param newCLient nuevo cliente que se va a agregar a la lista
     * @throws IOException al ingresar un dato erroneo o invalido
     */
    public void addClient(Client newCLient) throws IOException {
        Storage.writeInFile(CLIENTS_FILE, newCLient.toString());
    }

    /**
     * Obtiene una todos los clientes desde el origen de los datos
     *
     * @return lista de tipo {@link com.jimenezzj.models.Client} con todos los
     * clientes
     * @throws IOException al ingresar un dato erroneo o invalido
     */
    public List<Client> fetchClients() throws IOException {
        Path tmpPath = Paths.get(Storage.getFilesDir().concat(CLIENTS_FILE));
        return Files.lines(tmpPath)
                .filter(line -> !line.isBlank())
                .map(line -> {
                    List<String> datos = Storage.getDataFromString(line, "Cliente");
                    return new Client(datos.get(0), datos.get(1), datos.get(2), datos.get(3), datos.get(4));
                })
                .collect(Collectors.toList());

    }

}
