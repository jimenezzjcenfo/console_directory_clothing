package com.jimenezzj.controllers;

import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.jimenezzj.models.Catalog;
import com.jimenezzj.models.Shirt;
import com.jimenezzj.storage.CatalogStorage;
import com.jimenezzj.util.Storage;

/**
 * Se encarga de manejar los eventos disprados por el GUI en la clase
 * {@link com.jimenezzj.views.console.ViewConsole}}
 *
 * @author Jonathan Jimenez Zambrana
 * @since 1.0
 */
public class CatalogController {

    /**
     * Referencia a @{link com.jimenezzj.storage.CatalogStorage} para manejar los
     * metodos que almacenera los datos de forma persistente
     */
    private final CatalogStorage catalogStorage = new CatalogStorage();

    /**
     * Añade un catalogo al repositorio especificado
     *
     * @param pShirtsList lista de camisas en formato String
     * @throws IOException excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addCatalog(List<String> pShirtsList) throws IOException {
        List<Shirt> shirtsList = Storage.createShirtsList(pShirtsList);
        catalogStorage.addCatalog(new Catalog(shirtsList));
    }

    /**
     * Obtiene una lista de tipo {@link com.jimenezzj.models.Catalog} del repositorio
     *
     * @return Returna una lista de catalogos proveniento del repositorio
     * @throws IOException excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public List<String> getCatalogs() throws IOException {
        return catalogStorage.fetchAllCatalogs().stream()
                .map(Catalog::toString).collect(Collectors.toList());
    }

//    private void fillCatalog() {
//        catalogStorage.addCatalog(new Catalog(List.of(new Shirt(), new Shirt(), new Shirt())));
//        catalogStorage.addCatalog(new Catalog());
//        catalogStorage.addCatalog(new Catalog());
//        catalogStorage.addCatalog(new Catalog());
//    }

}
