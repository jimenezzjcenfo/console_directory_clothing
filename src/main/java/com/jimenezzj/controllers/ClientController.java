package com.jimenezzj.controllers;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.jimenezzj.models.Client;
import com.jimenezzj.storage.ClientStorage;

/**
 * Se encarga de manejar los eventos disprados por el GUI en la clase
 * {@link com.jimenezzj.views.console.ViewConsole} relacionados con
 * {@link com.jimenezzj.models.Catalog}
 *
 * @author Jonathan Jimenez Zambrana
 * @since 1.0
 */
public class ClientController {

    /**
     * Referencia a la clase {@link com.jimenezzj.storage.ClientStorage} encargada
     * de manejar acciones relacionadas con el repositorio de datos
     */
    private final ClientStorage clientDb = new ClientStorage();

    /**
     * Agrega nuevos clientes al repositorio de datos por medio de
     * {@link com.jimenezzj.storage.ClientStorage}
     *
     * @param name       nombre del cliente
     * @param firstName  primer apellido del cliente
     * @param adress     direccion del cliente
     * @param secondName segundo apellido del cliente
     * @param email      email del cliente
     * @throws IOException excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addNewClient(String name, String firstName, String secondName, String adress, String email)
            throws IOException {
        var newClient = new Client(name, firstName, secondName, adress, email);
        clientDb.addClient(newClient);
    }

    /**
     * Retorna un lista de clientes desde el repositorio de datos
     *
     * @return retorna lista de clientes
     * @throws IOException excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public List<String> getClientsList() throws IOException {
//        saveMockClients();
        return clientDb.fetchClients().stream()
                .map(Client::toString)
                .collect(Collectors.toList());
    }

    /**
     *
     */
    private void saveMockClients() {
//        clientDb.addClient(new Client());
//        clientDb.addClient(new Client());
//        clientDb.addClient(new Client());
    }

}
