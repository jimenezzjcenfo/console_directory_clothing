package com.jimenezzj.util;

import com.jimenezzj.models.Shirt;
import com.jimenezzj.models.ShirtSize;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Storage {
    public static String getFilesDir() {
        return System.getProperty("user.dir") + "\\files\\";
    }

    public static List<String> getDataFromString(String word, String typeToRemove) {
        return Arrays.stream(word.replace(",", "").split("\\w+=|\\[|]|" + typeToRemove))
                .filter(w -> !w.isBlank()).map(String::trim)
                .collect(Collectors.toList());
    }

    public static List<String> getDataFromString(String word) {
        return Arrays.stream(word.replace(",", "").split("\\w+=|\\[|]"))
                .filter(w -> !w.equals("")).map(String::trim)
                .collect(Collectors.toList());
    }

    public static void writeInFile(String fileName, String dataToWrite) throws IOException {
        var p = Paths.get(Storage.getFilesDir().concat(fileName));
        try (BufferedWriter bw = Files.newBufferedWriter(p, StandardOpenOption.APPEND)) {
            bw.newLine();
            bw.write(dataToWrite);
        } catch (IOException ioException) {
            ioException.printStackTrace();
            throw new IOException(ioException.getMessage());
        }
    }

    public static List<Shirt> createShirtsList(List<String> pShirtsList) {
        return pShirtsList.stream()
                .map(shText -> {
                    String[] shirtDatos = shText.split(" ");
//                    System.out.println(Arrays.asList(shirtDatos));
                    return new Shirt(Integer.parseInt(shirtDatos[0]), shirtDatos[1], shirtDatos[4],
                            new BigDecimal(shirtDatos[2]), ShirtSize.valueOf(shirtDatos[3].toUpperCase()));
                }).collect(Collectors.toList());
    }

    public static List<String> createStringChunksFromArray(String[] list, int chunkSize) {
        return IntStream.rangeClosed(1, (list.length / chunkSize)).boxed()
                .map(num -> IntStream.range((num * chunkSize) - chunkSize, (num * chunkSize)).boxed()
                        .map(n -> list[n])
                        .collect(Collectors.joining(" ")))
                .collect(Collectors.toList());
    }

}
