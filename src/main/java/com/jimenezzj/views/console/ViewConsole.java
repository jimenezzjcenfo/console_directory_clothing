package com.jimenezzj.views.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.jimenezzj.controllers.CatalogController;
import com.jimenezzj.controllers.ClientController;
import com.jimenezzj.models.Catalog;
import com.jimenezzj.models.Client;
import com.jimenezzj.models.Shirt;
import com.jimenezzj.models.ShirtSize;
import com.jimenezzj.util.Storage;

public class ViewConsole {
    private final ClientController clientController = new ClientController();
    private final CatalogController catalogController = new CatalogController();
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void showMainMenu() throws NumberFormatException, IOException {
        String menuText = "* * * * * * *\nMENU PRINCIPAL\n* * * * * * *\n" + "1) Catalogo\n" + "2) Clientes\n"
                + "3) Salir\n";
        int op = 0;
        while (op != 3) {
            System.out.println(menuText + "Seleccione un a opción");
            op = Integer.parseInt(reader.readLine());
            switch (op) {
                case 1:
                    showCatalogMenu();
                    break;
                case 2:
                    showClientsMenu();
                    break;
                case 3:
                    op = 3;
                    break;

                default:
                    System.out.println("Opcion in valida intente de nuevo.");
                    break;
            }
        }
    }

    private void showCatalogMenu() throws NumberFormatException, IOException {
        String menuText = "* * * * * * *\nCATALOGOS\n* * * * * * *\n" + "1) Ver lista de catalogos\n"
                + "2) Crear catalogo\n" + "3) Regresar\n";
        int op = 0;
        while (op != 3) {
            System.out.println(menuText + "Seleccione una opción");
            op = Integer.parseInt(reader.readLine());
            switch (op) {
                case 1:
                    showListCatalogsView();
                    break;
                case 2:
                    showAddCatalogView();
                    break;
                default:
                    System.out.println("Opcion in valida intente de nuevo.");
                    break;
            }
        }
    }

    private void showAddCatalogView() throws NumberFormatException, IOException {
        String subMenu = "En esta seccion se ingresan los items que formaran parte de un catalogo, seleccione una opcion:\n"
                + "1- Agregar un Item\n" + "2- Finalizar lista\n" + "3- Ver lista actual\n" + "4- Regresar";
        int op = 0;
        List<String> newCatalog = new ArrayList<>();
        while (op != 4) {
            System.out.println(subMenu);
            op = Integer.parseInt(reader.readLine());
            switch (op) {
                case 1:
                    System.out.println("Ingrese el codigo del item");
                    String idShirt = reader.readLine();
                    System.out.println("Ingrese el codigo del color del item");
                    String color = reader.readLine();
                    System.out.println("Ingrese la descripcion del item");
                    String desc = reader.readLine();
                    System.out.println("Ingrese el precio del item");
                    String price = reader.readLine();
                    System.out.println("Seleccione el tamano del item:\n" + "1- Small  " + "2- Medium  " + "3- Large  ");
                    int opList = Integer.parseInt(reader.readLine());
                    String size = "N";
                    if (opList == 1) size = "S";
                    if (opList == 2) size = "M";
                    if (opList == 3) size = "L";

                    try {
                        String shirt = String.format("%s %s %s %s %s", idShirt, color, price, size, desc);
                        newCatalog.add(shirt);
                        System.out.println("--> Se ingreso el item al catalogo con exito <--");
                    } catch (Exception e) {
                        System.out.println(
                                "- - - - - \tERROR\t - - - - -\n" + e.getMessage() + "\n - - - - - \tERROR\t - - - - -");
                        System.out.println("-> Intente registrar el item nuevamente con datos validos");
                    }

                    break;
                case 2:

                    catalogController.addCatalog(newCatalog);
                    System.out.println("--> Se agrego con exito el nuevo catalogo <--");
                    newCatalog = new ArrayList<>();
                    op = 4;
                    break;
                case 3:
                    System.out.println("- - - - - - - - - - - - - - - - \n|\t" + "Catalogo actual"
                            + "\t|\n- - - - - - - - - - - - - - - - ");
                    newCatalog.forEach(System.out::println);
                    System.out.println("- - - - - - - - - - - - - - - - \n|\t" + "Catalogo actual"
                            + "\t|\n- - - - - - - - - - - - - - - - ");
                    break;
                case 4:
                    op = 4;
                    newCatalog = new ArrayList<>();
                    System.out.println("--> Se cancelo la creacion del catalogo <--");
                    break;
                default:
                    System.out.println("--> Opcion invalida, intenta nuevamente <--");
                    break;
            }
        }

    }

    private void showListCatalogsView() throws NumberFormatException, IOException {
        int con = 1;
        int op;
        LinkedHashMap<Integer, String> tmpCatalogsList = new LinkedHashMap<>();
        System.out.println("- - - - - - - - - - - - - - - - \n" + "| LISTA DE CATALOGOS \t|\n"
                + "- - - - - - - - - - - - - - - - ");
        for (String c : catalogController.getCatalogs()) {
            tmpCatalogsList.put(con, c);
            System.out.println(con + ") " + c);
            con += 1;
        }
        System.out.println("- - - - - - - - - - - - - - - - \n" + "| LISTA DE CATALOGOS \t|\n"
                + "- - - - - - - - - - - - - - - - ");
        System.out.println("Ingrese el numero de la lista que desea seleccionar:\n" + "O pulse '0' para regresar");
        op = Integer.parseInt(reader.readLine());
        if (op > 0) {
            showChoosedCatalogView(tmpCatalogsList.get(op));
            showListCatalogsView();
        } else if (op < 0) {
            System.out.println("Esta opcion es invalida");
            showListCatalogsView();
        }
    }

    private void showChoosedCatalogView(String selectedCatalog) throws NumberFormatException, IOException {
        int op = 0;
        String menu = "Selecione una opcion:\n" + "1- Ver catalogo\n" + "2- Agregar items\n" + "3- Regresar";
        while (op != 3) {
            System.out.println(menu);
            op = Integer.parseInt(reader.readLine());
            switch (op) {
                case 1:
                    List<String> itemsCatalog = Storage.getDataFromString(selectedCatalog, "Catalogo");
                    System.out.println("- - - - - - - - - - - - - - - - \n|\t" + itemsCatalog.get(0)
                            + "\t|\n- - - - - - - - - - - - - - - - ");
//                    String[] test = itemsCatalog.get(2).split(" ");
                    Storage.createStringChunksFromArray(itemsCatalog.get(2).split(" "), 5)
                            .forEach(System.out::println);
                    System.out.println("- - - - - - - - - - - - - - - - \n|\t" + itemsCatalog.get(0)
                            + "\t|\n- - - - - - - - - - - - - - - - ");
                    break;
                case 2:
                    System.out.println("This function is over construction...");
                    break;
                case 3:
                    op = 3;
//				System.out.println();
                    break;
                default:
                    System.out.println("Esta opcion es invalida");
                    break;
            }
        }
    }

    private void showClientsMenu() throws NumberFormatException, IOException {
        String menuText = "1) Ver lista de clientes\n" + "2) Registrar un cliente\n" + "3) Salir\n";
        int op = 0;
        while (op != 3) {
            System.out.println(menuText);
            op = Integer.parseInt(reader.readLine());
            switch (op) {
                case 1:
                    System.out.println("- - - - - - - - - - - - \n" + "LISTA DE CLIENTES\n" + "- - - - - - - - - - - - ");
                    clientController.getClientsList().forEach(System.out::println);
                    System.out.println("- - - - - - - - - - - - \n" + "LISTA DE CLIENTES\n" + "- - - - - - - - - - - - ");
                    break;
                case 2:
                    System.out.println("Ingrese su nombre:");
                    String name = reader.readLine();
                    System.out.println("Ingrese su primer apellido:");
                    String fLastName = reader.readLine();
                    System.out.println("Ingrese su segundo apellido:");
                    String sLastName = reader.readLine();
                    System.out.println("Ingrese su email:");
                    String email = reader.readLine();
                    System.out.println("Ingrese la direccion de su vivienda:");
                    String address = reader.readLine();
                    clientController.addNewClient(name, fLastName, sLastName, email, address);
                    System.out
                            .println("- - - - - - - - -\n " + "| Usuario añadido exitosamente! |\n" + "- - - - - - - - - ");
                    break;
                default:
                case 3:
                    op = 3;
                    System.out.println("Opcion invalida, intentelo nuevamente");
                    break;
            }
        }
    }

}
