package com.jimenezzj.models;

import java.util.Hashtable;

public enum ShirtSize {
	N(0, "Ninguna"), L(3, "Large"), M(2, "Medium"), S(1, "Small");

	private static Hashtable<Integer, ShirtSize> shirtSizeList = new Hashtable<>();

	static {
		for (ShirtSize size : ShirtSize.values()) {
			shirtSizeList.put(size.getSizeNum(), size);
		}
	}

	private int numRepresentation;
	private String fullName;

	private ShirtSize(int numRepresentation, String fullName) {
		this.numRepresentation = numRepresentation;
		this.fullName = fullName;
	}

	public int getSizeNum() {
		return this.numRepresentation;
	}

	public String getFullName() {
		return this.fullName;
	}

	public static ShirtSize getSizeWithCode(int sizeNum) {
		return shirtSizeList.get(sizeNum);
	}
}
