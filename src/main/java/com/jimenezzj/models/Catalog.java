package com.jimenezzj.models;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Representa un catalogo.
 * Ver {@link com.jimenezzj.models.Shirt} para mas
 * detalles del tipo de la lista
 *
 * @author Jonathan Jimenez Zambrana
 * @version 1.0
 */
public class Catalog {

    private String name;
    private LocalDate createdAt;

    /**
     * Contiene los objetos de tipo {@link com.jimenezzj.models.Shirt}
     */
    private List<Shirt> itemsList;

    /**
     * Crea un catalogo con los atributos especificados
     * @param name nombre del catalogo
     * @param createdAt fecha de creacion del catalogo
     * @param itemsList items que forman parte del catalogo de tipo {@link com.jimenezzj.models.Shirt}
     *
     */
    public Catalog(String name, LocalDate createdAt, List<Shirt> itemsList) {
        this.name = name;
        this.createdAt = createdAt;
        this.itemsList = itemsList;
    }

    /**
     * Crea un catalogo con los atributos especificados
     *
     * @param itemsList Liste de items que contiene
     */
    public Catalog(List<Shirt> itemsList) {
        setName(generateName());
        this.createdAt = LocalDate.now();
        this.itemsList = itemsList;
    }

    /**
     * Crea un catalogo con los valores for defecto
     */
    public Catalog() {
        this(new ArrayList<Shirt>());
    }

    /**
     * Genera una representacion de tipo string, que incluye las propiedades del
     * catalogo
     *
     * @return Un string con todos las propiedades de un catalogo
     */
    @Override
    public String toString() {
        return String.format("Catalogo[nombre=%s, creado=%s, totalItems=%s]", getName(), getCreatedAt(), getItemsList().toString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCreatedAt(), getItemsList());
    }

    /**
     * Establece los criterio a utilizar cuando se realiza una comparación con otro
     * objeto del mismo tipo
     *
     * @param o Objeto con el que se hara la comparacion
     * @return un Booleano que representa si el objeto es igual al del parametro obj
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Catalog)) return false;
        Catalog catalog = (Catalog) o;
        return getName().equals(catalog.getName()) &&
                getCreatedAt().isEqual(catalog.getCreatedAt());
    }

    /**
     * Genera el nombre de la lista en base al mes y año actual
     *
     * @return returna un String con el nombre del catalogo
     */
    protected String generateName() {
        LocalDate currentDate = LocalDate.now();
        String month = currentDate.getMonth().name().substring(0, 1).toUpperCase()
                + currentDate.getMonth().name().substring(1).toLowerCase();
        Integer year = currentDate.getYear();
        return month + "-" + year;
    }

    /**
     * Agregar un objeto de tipo Shirt a lista del catalogo
     *
     * @param newShirt El objeto de tipo camisa que se agrgara a la lista del
     *                 catalogo
     * @throws Exception Lanza una excepcion cuando se intenta agregar un elemento repetido a
     *               la lista
     */
    public void addItem(Shirt newShirt) throws Exception {
        LinkedList<Shirt> catalogHasSet = new LinkedList<>(getItemsList());
        if (catalogHasSet.contains(newShirt)) {
            throw new Exception("Este item ya se encuentra registrado");
        } else {
            getItemsList().add(newShirt);
        }
    }

    /**
     * Obtiene la lista del catalogo. Objetos de la lista son de tipo
     * {@link com.jimenezzj.models.Shirt}}
     *
     * @return retorna lista con todos los items
     */
    public List<Shirt> getItemsList() {
        return itemsList;
    }

    private void setItemsList(List<Shirt> newItemsList) {
        this.itemsList = newItemsList;
    }

    /**
     * Obtiene el nombre del catalogo
     *
     * @return returna un String con el nombre del catalogo
     */
    public String getName() {
        return name;
    }

    /**
     * Cambia el nombre del catalogo
     *
     * @param name el nombre nuevo del catalogo
     */
    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    private void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

}
