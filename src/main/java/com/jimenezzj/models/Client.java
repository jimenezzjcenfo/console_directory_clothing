package com.jimenezzj.models;

/**
 * Es la representacion de un cliente.
 *
 * @author Jonathan Jimenez Zambrana
 * @version 1.0
 */
public class Client {
    private String name;
    private String firstName;
    private String secondName;
    private String adress;
    private String email;

    /**
     *  Crea un cliente con los atributos que se especifiquen
     * @param name valor del nombre del cliente
     * @param firstName valor del primer apellido del cliente
     * @param secondName valor del secundo nombre del cliente
     * @param adress valorde de la direccion del cliente
     * @param email valor del email del cliente
     */
    public Client(String name, String firstName, String secondName, String adress, String email) {
        this.name = name;
        this.firstName = firstName;
        this.secondName = secondName;
        this.adress = adress;
        this.email = email;
    }

    /**
     * Crea un cliente con las propiedades por defecto
     */
    public Client() {
        this("Sin nombre", "Sin primer apellido", "Sin segunbdo apellido", "Sin direccion", "Sin email");
    }

    /**
     * Genera una representacion de tipo String con todas las propieades
     * del cliente
     *
     * @return representacion de tipo String del cliente
     */
    @Override
    public String toString() {
        return String.format("Cliente[nombre=%s, PrimerApellido=%s, SegundoApellido=%s, Email=%s, Direccion=%s]",
                getName(), getFirstName(), getSecondName(), getEmail(), getAdress());
    }

    /**
     * Obtiene el nombre del cliente
     *
     * @return String del nombre del cliente
     */
    public String getName() {
        return name;
    }

    /**
     * Cambia el nombre del cliente por el especificado en el parametro name
     *
     * @param name el nuevo nombre del cliente
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
