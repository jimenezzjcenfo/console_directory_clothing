package com.jimenezzj.models;

import java.math.BigDecimal;

/**
 * Es un a representacion de una camiseta. Ver la enumo
 * {@link com.jimenezzj.models.ShirtSize} para mas detalles
 *
 * @author Jonathan Jimenez Zambrana
 * @version 1.0
 */
public class Shirt {

    private int itemId;
    private String codeColor;
    private String description;
    private BigDecimal price;
    /**
     * El tamaño de la camisa representado por una enum
     * {@link com.jimenezzj.models.ShirtSize}
     */
    private ShirtSize size;

    /**
     * Crea una camisa con los atributos especificados
     *
     * @param itemId      el id de la camisa
     * @param codeColor   codigo del colorde la camisa
     * @param description la descricion de la camisa
     * @param price       el precio de la camisa
     * @param size        el tamano de la camisa
     */
    public Shirt(int itemId, String codeColor, String description, BigDecimal price, ShirtSize size) {
        this.itemId = itemId;
        this.codeColor = codeColor;
        this.description = description;
        this.price = price;
        this.size = size;
    }

    /**
     * Crea una camisa con el valor de las propiedades por defecto
     */
    public Shirt() {
        this(0, "Sin color", "Sin descripcion", new BigDecimal("0.00"), ShirtSize.N);
    }

    /**
     * Representacion de tipo String de una camisa con todos sus propiedades.
     * Sobreescribe el metodo por defecto
     *
     * @return String de la camisa con todas sus propiedades
     */
    @Override
    public String toString() {
        return String.format("%s, %s, %s, %s, %s", getItemId(), getCodeColor(), getPrice().toString(),
                getSize(), getDescription());
    }

    /**
     * Determina si el objeto es igual a otro del mismo tipo
     *
     * @param obj objeto con el que se realizara la comparacion
     * @return resultado de comparar el objeto actual con el especificado en el
     * parametro
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass())
            return false;
        if (this == obj)
            return true;
        Shirt comparableObj = (Shirt) obj;
        return getItemId() == comparableObj.getItemId() || getCodeColor().equals(comparableObj.getCodeColor())
                && getPrice().equals(comparableObj.getPrice()) && getSize() == comparableObj.getSize()
                && getDescription().equals(comparableObj.getDescription());
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getCodeColor() {
        return codeColor;
    }

    public void setCodeColor(String codeColor) {
        this.codeColor = codeColor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Obtiene el tamaño de la camisa representado por
     * {@link com.jimenezzj.models.ShirtSize}
     *
     * @return return el tamano de la camisa
     */
    public ShirtSize getSize() {
        return size;
    }

    /**
     * Cambia el valor actual del tamaño de la camisa
     *
     * @param size Ajusta el valor del tamano de la camisa
     */
    public void setSize(ShirtSize size) {
        this.size = size;
    }

}
