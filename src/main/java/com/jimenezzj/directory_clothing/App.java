package com.jimenezzj.directory_clothing;

import java.io.IOException;
import java.time.LocalDate;

import com.jimenezzj.models.Shirt;
import com.jimenezzj.models.ShirtSize;
import com.jimenezzj.views.console.ViewConsole;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class App extends Application {

	Stage stage;
	Button btnConsoleView;
	Button btnGUIView;

	@Override
	public void start(Stage primaryStage) {
//		System.out.println(ShirtSize.);
		stage = primaryStage;
		
		Label lblAppVersion = new Label("Seleccione el modo en el que desea iniciar la aplicacion");
		lblAppVersion.setFont(new Font(13));

		// creating buttons
		btnConsoleView = new Button("Consola");
		btnConsoleView.setPrefWidth(100);
		btnConsoleView.setFont(new Font(15));
		btnConsoleView.setOnAction(e -> closeGUI());

		btnGUIView = new Button("Interfaz Grafica");
		btnGUIView.setMinWidth(100);
		btnGUIView.setFont(new Font(15));
		btnGUIView.setOnAction(e -> openGUI());

		HBox btnsPannel = new HBox(25);
		btnsPannel.setPadding(new Insets(20));
		Region spacerBtns = new Region(), spacerBtnsMid = new Region(), spacerBtnsEnd = new Region();
		HBox.setHgrow(spacerBtns, Priority.ALWAYS);
		HBox.setHgrow(spacerBtnsMid, Priority.ALWAYS);
		HBox.setHgrow(spacerBtnsEnd, Priority.ALWAYS);
		btnsPannel.getChildren().addAll(spacerBtns, btnConsoleView, btnGUIView, spacerBtnsEnd);

		VBox mainContainer = new VBox(20, lblAppVersion, btnsPannel);
		mainContainer.setAlignment(Pos.TOP_CENTER);
		mainContainer.setPadding(new Insets(15, 10, 10, 10));

		var scene = new Scene(mainContainer);
		primaryStage.setTitle("Directory Clothing App");
		primaryStage.setMinHeight(200);
		primaryStage.setMinWidth(400);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private Object openGUI() {
		// TODO Auto-generated method stub
		return null;
	}

	private void closeGUI() {
		ViewConsole consoleView = new ViewConsole();
		stage.close();
		try {
			System.out.println("\n\n\n\n");
			consoleView.showMainMenu();
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch();
	}

}